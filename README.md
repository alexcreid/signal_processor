# Signal Processing Service
Alex Reid (hello@alexreid.org)

This is a simple Flask app that implements the beginnings of a signal
processing service, which currently is capable of calculating the number of
times a signal crosses a given threshold value. All requests to the service
are made as HTTP requests with JSON bodies.

The flow of using the service looks like this:

```
--> POST to /api/get_number_of_value_crossings:
X-API-Version: 1.0
Body:
{
    "signal": [1, 2, 3, 4, 5, 4, 3, 2],
    "value": 3
}

<-- Response 200 OK
Body:
{
    "result": 2
}
```

If the signal or value keys are missing from the request, a 400 Bad Request
response will be given. Clients must also specify that they require API version
1.0 by setting the X-API-Version header as above, or a 400 Bad Request response
will be given. Clients must also authorise using HTTP Basic Auth, or a 401
Unauthorized response will be given.

The valid usernames and passwords are configured in config.py -- by default,
you can use 'username' and 'password'. This would hopefully be changed for
the real deployed app!

## Installation

### Development Server
Prerequisites: Python 3.6 or above

The Flask app can be run directly in development mode. Ensure you're working
in the top-level directory of the repository (the one containing this readme).
With a Python 3.6 virtualenv active, install the requirements with
`pip install -r requirements.txt`. Then simply run the service as follows:

```
FLASK_APP=signal_processor_svc FLASK_ENV=development flask run
```

The service will be accessible at http://localhost:5000/. Note that running
with `FLASK_ENV=development` will limit the service to servning a single
request at a time.

If you have [HTTPie](https://httpie.org) installed, you can easily poke at the API for ad-hoc testing, for
example:

```
 > http --auth "username:password" post http://127.0.0.1:5000/api/get_number_of_value_crossings signal:="[1, 2, 3, 4, 3, 2]" value:="3" X-API-Version:1.0

HTTP/1.1 200 OK
{
    "result": 2
}
```

### Docker Container
Prerequisites: Docker (tested with engine version 18.09.1)

A simple docker container can be built automatically, which runs the service
using Gunicorn as the WSGI server. This could then be used with Kubernetes to
facilitate scaling.

Change directory to ops/docker/, then run build.sh. The container will be built
automatically, and exposes the HTTP server on port 8000. It can be run like
this for example:

```
docker -p 8000:8000 run <container ID from the build script output>
```

(The Docker host will then be able to connect to the server on localhost:8000.)

### Testing
A few automated unit tests for the calculator function are included -- in
reality we'd want there to be more tests, especially of auth and the API
endpoints! Tests can be run using pytest from the root of the repository:

```
# From within the virtualenv!
pip install pytest
pytest
```

## Design Decisions

- By structuring the response as a JSON object rather than a bare integer,
  we leave the door open for extending the response in the future.
- Clients must specify that they're expecting API version 1.0, which means that
  in the future the API version header could be used to route clients
  automatically to the latest compatible version of the endpoint.
- By structuring the service as an HTTP app running via Flask, a lightweight
  Python web framework, we allow simple handling of concurrent requests and
  easy scaling supported by a huge amount of existing open-source tooling.
  Since almost anything can make JSON HTTP requests, this makes it easy for
  any client to make use of this service.

## Future Improvements

- If very large, or continuously-generated signals are expected, I'd instead
  consider implementing the value crossing calculator so that clients can
  continously stream signal values, possibly using Websockets via Python's
  asyncio. The actual function that calculates the number of crossings only
  requires memory for the next signal value and a boolean, and can be given
  any iteratable value, so it would be easy to re-use that for a streaming
  implementation.
- Many more tests would be really nice!
- I'd further refine the container config and explore how to make it scale
  in production.
- More input validation would be useful -- currently 500s happen if numbers
  are given as strings!
- API discoverability could be much better -- we could auto-generate docs
  and API schemas, and provide a hyperlinked API root that clients can
  navigate from.
