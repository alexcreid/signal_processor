from numbers import Real
from typing import Iterable, Optional


def calculate_number_of_value_crossings(signal: Iterable[Real],
                                        trigger_value: Real) -> int:
    """
    Given a signal as an iterable of real numbers, calculate the number of
    times the signal crosses the given trigger value.
    :param signal: an iterable of real numbers representing the signal.
    :param trigger_value: the value over which the number of crossing events
        will be calculated.
    """
    # last_side represents on which side of the trigger value the signal
    # currently is. We use None to signify that we don't yet know what side
    # the signal is on -- if the signal starts precisely on the trigger value
    # which side we're on is undefined until the value changes.
    last_side: Optional[int] = None
    current_side: Optional[int]
    crossings = 0
    for current_value in signal:
        if current_value < trigger_value:
            current_side = -1
        elif current_value > trigger_value:
            current_side = 1
        else:
            current_side = last_side
        if current_side != last_side and last_side is not None:
            crossings += 1
        last_side = current_side
    return crossings
