from functools import wraps

from flask import Blueprint, jsonify, request

from signal_processor_svc.auth import require_auth
from signal_processor_svc import calculators


bp = Blueprint('api', __name__, url_prefix='/api')


def require_version(required_version):
    """
    Require client to set an X-API-Version header with the value specified
    before a request will be passed to the wrapped view.
    """
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            invalid_version_response = (
                f'The X-API-Version header must be set to {required_version}',
                400)
            try:
                requested_version = request.headers['X-API-Version']
            except KeyError:
                return invalid_version_response

            if requested_version != required_version:
                return invalid_version_response
            else:
                return f(*args, **kwargs)
        return wrapper
    return decorator


# We'll simply required that all clients specify version 1.0 of the API for
# now -- this will allow us to make potentially breaking changes later, and
# we can simply provide the older version for clients still specifying the
# original version.
@bp.route('/get_number_of_value_crossings', methods=('POST',))
@require_auth
@require_version('1.0')
def get_number_of_value_crossings():
    try:
        signal = request.json['signal']
    except KeyError:
        return('The request must include the signal under the "signal" key.',
               400)

    try:
        value = request.json['value']
    except KeyError:
        return ('The request must include the trigger value under the '
                '"value" key.', 400)

    result = calculators.calculate_number_of_value_crossings(
        signal=signal, trigger_value=value)

    # A standard 200 OK response seems like the most reasonable thing here.
    # We'll return a JSON object to leave the possibility open of adding more
    # data later under other keys without breaking existing clients.
    return jsonify(result=result)
