from functools import wraps

from flask import current_app, request


def require_auth(f):
    """Require a client to authenticate via HTTP Basic authentication."""
    @wraps(f)
    def wrapper(*args, **kwargs):
        auth = request.authorization
        if not auth or not _check_credentials(auth.username, auth.password):
            return (
                'No (or incorrect) authorization details given.', 401,
                {'WWW-Authenticate': 'Basic realm="Log in to the signal '
                                     'processor service."'})
        else:
            return f(*args, **kwargs)
    return wrapper


def _check_credentials(username: str, password: str) -> bool:
    authorized_users = current_app.config['AUTHORIZED_USERS']
    try:
        correct_password = authorized_users[username]
    except KeyError:
        return False

    return password == correct_password
