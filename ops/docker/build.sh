#!/bin/bash

echo "Building app directory structure..."
mkdir app/
cp ../../requirements.txt app/
cp -R ../../signal_processor_svc app/

echo "Building Docker container..."

docker build .

echo "Cleaning up..."
rm -rf app/

echo 'Done!'
