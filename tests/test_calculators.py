from signal_processor_svc.calculators import \
    calculate_number_of_value_crossings


class TestCalculateNumberOfValueCrossings:

    """Tests for the `calculate_number_of_value_crossings` function."""

    def test_counting_number_of_crossings(self):
        """The correct number of crossings is returned."""
        signal = [1, 2, 3, 2, 3, 4, 3, 2, 1, 2, 3, 4]
        result = calculate_number_of_value_crossings(signal, 3)
        assert result == 3

    def test_counting_number_of_crossings_starting_on_value(self):
        """
        The correct number of crossings is returned if we start out on the
        trigger value.
        """
        signal = [3, 4, 5, 4, 3, 2, 1, 2]
        result = calculate_number_of_value_crossings(signal, 3)
        assert result == 1

    def test_counting_number_of_crossings_with_empty_signal(self):
        """If the signal is empty, the number of crossings is zero."""
        result = calculate_number_of_value_crossings([], 3)
        assert result == 0

    def test_counting_number_of_crossings_with_floats(self):
        """The signal and trigger value may be floating point numbers."""
        signal = [1.1, 1.2, 1.3, 1.2, 1.3, 1.4, 1.3, 1.2, 1.1, 1.2, 1.3, 1.4]
        result = calculate_number_of_value_crossings(signal, 1.3)
        assert result == 3

    def test_counting_number_of_crossings_with_negative_numbers(self):
        """The signal and trigger values may be negative numbers."""
        signal = [-1, -2, -3, -2, -3, -4, -3, -2, -1, -2, -3, -4]
        result = calculate_number_of_value_crossings(signal, -3)
        assert result == 3

    def test_counting_number_of_crossings_with_no_crossings(self):
        """If the signal never crosses the trigger value, 0 is returned."""
        signal = [10, 11, 12, 10, 9, 5]
        result = calculate_number_of_value_crossings(signal, 1)
        assert result == 0
